import 'package:flutter/material.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Remove padding
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text('63160283'),
            accountEmail: Text('63160283@go.buu.ac.th'),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.network(
                  'https://scontent.fbkk23-1.fna.fbcdn.net/v/t1.6435-9/119801781_2647535865495143_3237071145685744674_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeGAMkafvEXr09CPn9WWcgAhMlYb13DsIe4yVhvXcOwh7g0u9iwevlkIgrIAK48K915yfn28AeLu_W-9bWJDIHc2&_nc_ohc=mx-biVPVW1oAX-ybXpq&_nc_ht=scontent.fbkk23-1.fna&oh=00_AfDLcj93SyPHKG6whMlZPca_dC1YjiPqSm5D_Ihj8bswpw&oe=6401B02E',
                  fit: BoxFit.cover,
                  width: 90,
                  height: 90,
                ),
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.grey.shade800,
              image: DecorationImage(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ5Ju7fWJ7Om0SiWvSNeLhdfccvu9xkhwCVuA&usqp=CAU'),scale: 1),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('เมนูหลัก'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.notifications),
            title: Text('ลงทะเบียน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.calendar_today),
            title: Text('ตารางเรียน'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.face),
            title: Text('ประวัตินิสิต'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.quick_contacts_mail_rounded),
            title: Text('ผลการศึกษา'),
            onTap: () => null,
          ),

          Divider(),
          ListTile(
            leading: Icon(Icons.monetization_on),
            title: Text('ค่าใช้จ่าย'),
            onTap: () => null,
          ),
          ListTile(
            leading: Icon(Icons.description),
            title: Text('ยื่นคำร้อง'),
            onTap: () => null,
          ),
          Divider(),
          ListTile(
            title: Text('เกี่ยวกับ'),
            leading: Icon(Icons.settings),
            onTap: () => null,
          ),
          ListTile(
            title: Text('ข้อเสนอแนะ'),
            leading: Icon(Icons.report_problem_outlined),
            onTap: () => null,
          ),
          ListTile(
            title: Text('ออกจากระบบ'),
            leading: Icon(Icons.exit_to_app),
            onTap: () => null,
          ),
        ],
      ),
    );
  }
}

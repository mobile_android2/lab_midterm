import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'login_page.dart';
import 'NavBar.dart';
import 'constrained_box_example.dart';

void main() {
  runApp(const ColumnExample());
}

class ColumnExample extends StatelessWidget {
  const ColumnExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'REG Burapha University',
        theme: ThemeData(
            // primarySwatch: Colors.grey,

            ),
        home: Scaffold(
          drawer: NavBar(),
          backgroundColor: Colors.white,
          appBar: AppBar(

            title:  const Text(' มหาวิทยาลัย บูรพา\nBurapha University',
                style:TextStyle(color: Colors.white)),
            backgroundColor: Colors.grey,
          ),
          body: const SafeArea(
            child: ConstrainedBoxExample(),
          ),
        ),
      ),
    );
  }
}
